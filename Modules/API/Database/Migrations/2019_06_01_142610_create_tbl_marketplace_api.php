<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblMarketplaceApi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketplace_api', function (Blueprint $table) {
            $table->increments('marketplace_api_id');
            $table->integer('marketplace_id')->unsigned()->comment('FK:marketplace>marketplace_id');
            $table->string('api_key', 50);
            $table->string('api_description', 50);
            $table->integer('access_token_id')->comment('FK:access_token>access_token_id');
            $table->boolean('is_active');
            $table->integer('update_by_user_id')->unsigned()->comment('FK:user>user_id')->nullable();
            $table->integer('create_by_user_id')->unsigned()->comment('FK:user>user_id')->nullable();
            $table->timestamp('updated_at');
            $table->timestamp('created_at')->useCurrent();
        });

        Schema::table('marketplace_api', function($table) {
            $table->foreign('marketplace_id')->references('marketplace_id')->on('marketplace');
            $table->foreign('create_by_user_id')->references('user_id')->on('user');
            $table->foreign('update_by_user_id')->references('user_id')->on('user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marketplace_api');
    }
}
