<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblAccessTokenApi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('access_token', function (Blueprint $table) {
            $table->increments('access_token_id');
            $table->string('access_token', 10);
            $table->string('refresh_token', 10);
            $table->string('country', 10);
            $table->integer('code');
            $table->string('account_platform', 50);
            $table->integer('refresh_expires_in');
            $table->integer('expires_in');
            $table->integer('user_id');
            $table->integer('seller_id');
            $table->string('short_code', 50);
            $table->string('account', 50);
            $table->boolean('is_active');
            $table->integer('update_by_user_id')->unsigned()->comment('FK:user>user_id')->nullable();
            $table->integer('create_by_user_id')->unsigned()->comment('FK:user>user_id')->nullable();
            $table->timestamp('updated_at');
            $table->timestamp('created_at')->useCurrent();
        });

        // Schema::table('access_token', function($table) {
        //     $table->foreign('create_by_user_id')->references('user_id')->on('user');
        //     $table->foreign('update_by_user_id')->references('user_id')->on('user');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('access_token');
    }
}
