<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblArmsApi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arms_api', function (Blueprint $table) {
            $table->increments('arms_api_id');
            $table->string('url', 50);
            $table->string('integration_code', 100);
            $table->string('x_access_token', 50);
            $table->boolean('is_active');
            $table->integer('update_by_user_id')->unsigned()->comment('FK:user>user_id')->nullable();
            $table->integer('create_by_user_id')->unsigned()->comment('FK:user>user_id')->nullable();
            $table->timestamp('updated_at');
            $table->timestamp('created_at')->useCurrent();
        });

        // Schema::table('arms_api', function($table) {
        //     $table->foreign('create_by_user_id')->references('user_id')->on('user');
        //     $table->foreign('update_by_user_id')->references('user_id')->on('user');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arms_api');
    }
}
