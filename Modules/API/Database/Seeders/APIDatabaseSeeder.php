<?php

namespace Modules\API\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class APIDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $exist_market = DB::table('marketplace')->exists();
        if(!$exist_market){
            $marketplace_id = DB::table('marketplace')->insertGetId([
                'name' => 'Lazada',
                'is_active' => 1
            ]);
        }
    }
}
