<?php

namespace Modules\API\Entities;

use Illuminate\Database\Eloquent\Model;

class Marketplace extends Model
{
   	protected $table = 'marketplace';
    protected $primaryKey = 'marketplace_id';
    protected $fillable = ['*'];


}
