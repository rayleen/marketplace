@extends('template::layouts.default')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
   <div class="container-fluid">
      @if($errors->any())
      <div class="alert alert-danger">
        <b>{{$errors->first()}}</h4>
      </div>
      @endif
    </div>
          <!-- /.container-fluid -->
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard abcd
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

    </section>
    <!-- /.content -->
  </div>
@stop
