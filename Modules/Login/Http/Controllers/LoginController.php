<?php

namespace Modules\Login\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

use Session;
use Validator;
use Modules\User\Entities\User;
use Modules\API\Entities\Marketplace;
use Modules\Settings\Entities\UITheme;

class LoginController extends Controller
{
    
    function __construct()
    {
       
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        /**
         * Get theme, add to session
        */
        $default = UITheme::where('is_default', 1)->first();
        $css_name = $default->css_file_name;
        Session::put('css_file_name', $css_name);

        return view('login::index', ['css_file_name' => $css_name]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('login::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('login::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('login::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }



    public function checklogin(Request $request)
    {
        $user_name = $request->username;
        $password = $request->password;

        $validator = Validator::make($request->all(), [
            'username' => 'required|min:5|max:50',
            'password' => 'required|min:5|max:50',
        ]);


        if($validator->fails()){
            $validator->getMessageBag()->add('validation', 'Invalid input.');
            return redirect('/')->withErrors($validator);
        }else{
            if (Auth::attempt(['login_name' => $user_name, 'password' => $password]))
            {
                /**
                 * Check if markeplace is active
                 */
                $markeplace = Marketplace::find(1);
                    if($markeplace->is_active == 1){
                    /**
                     * Check if branch is active
                     */
                    $user = User::find(Auth::user()->user_id);
                    $active = $user->branch->is_active;
                    if($active == 1){
                        return redirect()->route('dashboard');
                    }else{
                        $validator->getMessageBag()->add('validation', 'Your branch has been deactivated. Please contact admin.');
                        return redirect('/')->withErrors($validator);
                    }
                }else{
                    $validator->getMessageBag()->add('validation', 'The markeplace has been deactivated.');
                    return redirect('/')->withErrors($validator);
                }

            }else{
                $validator->getMessageBag()->add('validation', 'The username or password is incorrect.');
                return redirect('/')->withErrors($validator);
            }
        }

    }
    public function logout()
    {
        Session::flush();
        Auth::logout();
        return redirect()->route('login');
    }

    public function test()
    {
        echo 'test';
    }
    
}
