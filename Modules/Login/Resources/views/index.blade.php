@extends('template::layouts.login.login-mainlayout')

@section('content')
  <header class="fontc masthead">
    <div class="container h-100">
      <div class="row h-100">
        <div class="col-lg-7 my-auto">
          <div class="header-content mx-auto">
            <h1 class="mb-5"><span style="font-weight: bold">Lorem Ipsum</span><br>New Age is an app landing page that will help you beautifully showcase your new mobile app, or anything else!</h1>
             <div class="col-12 col-md-6">
                <a href="#download" class="btn2 btn btn-block btn-lg btn-primary">Learn More</a>
              </div>
          </div>
        </div>
        <div class="col-lg-4 my-auto signin" >
          <div class="mx-auto">
          <center><strong><h3 class="mb-5" style="font-weight: bold">SIGN-IN</h3></strong></center>
          </div>
           <form method="POST" action="{{route('check_login')}}">
                      <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                  <div class="form-group">
                    <div class="form-label-group">
                      <input type="text" id="inputEmail" name="username" class="form-control" placeholder="Username" required="required" autofocus="autofocus">
                    </div>
                </div>
                  <div class="form-group">
                    <div class="form-label-group">
                      <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" value="remember-me">
                        Remember Password
                      </label>
                    </div>
                  </div>
                  <button type="submit" class="btn btn-primary btn-block" name="login">Login</button>
              </form>
              <div class="text-center reg">
                <a class="d-block small mt-3" href="register.html">Register an Account</a>
                <a target="_blank" class="d-block small" href="{{route('forgot_password')}}">Forgot Password?</a>
              </div>
              <div class="container-fluid">
              @if($errors->any())
                      <div class="alert alert-danger">
                        <b>{{$errors->first()}}</h4>
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                      </div>
                      @endif

                      @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                    </div>
              @endif
          </div>
        </div>
      </div>
    </div>
  </header>
@stop

@section('ads')
  <section class="contact bg-primary" id="contact">
    <div class="container">
      <h2>ADS
        ADS</h2>
    </div>
  </section>
@stop