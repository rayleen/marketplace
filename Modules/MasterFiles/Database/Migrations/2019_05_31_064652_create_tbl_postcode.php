<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblPostcode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('postcode', function (Blueprint $table) {
            $table->string('postcode', 10);
            $table->string('area_name', 50);
            $table->integer('state_id')->unsigned()->comment('FK:state->state_id');
            $table->boolean('is_active');
            $table->integer('update_by_user_id')->unsigned()->comment('FK:user>user_id')->nullable();
            $table->timestamp('updated_at');
            $table->integer('create_by_user_id')->unsigned()->comment('FK:user>user_id')->nullable();
            $table->timestamp('created_at')->useCurrent();

            $table->primary('postcode'); 
        });

        Schema::table('postcode', function($table) {
            $table->foreign('state_id')->references('state_id')->on('state');
            $table->foreign('create_by_user_id')->references('user_id')->on('user');
            $table->foreign('update_by_user_id')->references('user_id')->on('user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('postcode');
    }
}
