<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblImage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image', function (Blueprint $table) {
            $table->increments('image_id');
            $table->string('name', 50);
            $table->string('type', 50);
            $table->integer('size');
            $table->binary('image_file');
            $table->integer('update_by_user_id')->unsigned()->comment('FK:user>user_id')->nullable();
            $table->timestamp('updated_at');
            $table->integer('create_by_user_id')->unsigned()->comment('FK:user>user_id')->nullable();
            $table->timestamp('created_at')->useCurrent();
        });

        Schema::table('image', function($table) {
            $table->foreign('create_by_user_id')->references('user_id')->on('user');
            $table->foreign('update_by_user_id')->references('user_id')->on('user');
        });

        Schema::table('user', function($table) {
            DB::statement("ALTER TABLE `user` CHANGE COLUMN `image_id` `image_id` INT(11) UNSIGNED");
            $table->foreign('image_id')->references('image_id')->on('image');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image');
        Schema::table('user', function($table) {
            $table->dropForeign(['image_id']);
        });
    }
}
