<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permission', function (Blueprint $table) {
            $table->tinyInteger('permission_id')->unsigned()->comment('1=View,2=Add,3=Edit, 4=Delete');
            $table->string('name', 50);
            $table->boolean('is_active');
            $table->integer('update_by_user_id')->unsigned()->comment('FK:user>user_id')->nullable();
            $table->timestamp('updated_at');
            $table->integer('create_by_user_id')->unsigned()->comment('FK:user>user_id')->nullable();
            $table->timestamp('created_at')->useCurrent();

            $table->primary('permission_id'); 
        });

        Schema::table('permission', function($table) {
            $table->foreign('create_by_user_id')->references('user_id')->on('user');
            $table->foreign('update_by_user_id')->references('user_id')->on('user');
        });

        Schema::table('role_menu_permission', function($table) {
            DB::statement("ALTER TABLE `role_menu_permission` CHANGE COLUMN `permission_id` `permission_id` tinyInt(11) UNSIGNED");
            $table->foreign('permission_id')->references('permission_id')->on('permission');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('role_menu_permission', function($table) {
            $table->dropForeign(['permission_id']);
        });
        Schema::dropIfExists('permission');
    }
}
