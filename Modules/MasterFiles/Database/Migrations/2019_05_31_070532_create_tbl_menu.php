<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu', function (Blueprint $table) {
            $table->increments('menu_id');
            $table->string('name', 50);
            $table->integer('parent_menu_id')->unsigned()->comment('FK:menu>menu_id')->nullable();
            $table->boolean('level')->comment('to indicate if this menu_id is the 1st level 1-1st Level menu, 0-sub menu');
            $table->integer('sequence');
            $table->boolean('is_active');
            $table->integer('update_by_user_id')->unsigned()->comment('FK:user>user_id')->nullable();
            $table->timestamp('updated_at');
            $table->integer('create_by_user_id')->unsigned()->comment('FK:user>user_id')->nullable();
            $table->timestamp('created_at')->useCurrent();
        });

        Schema::table('menu', function($table) {
            $table->foreign('parent_menu_id')->references('menu_id')->on('menu');
            $table->foreign('create_by_user_id')->references('user_id')->on('user');
            $table->foreign('update_by_user_id')->references('user_id')->on('user');
        });

        Schema::table('role_menu', function($table) {
            DB::statement("ALTER TABLE `role_menu` CHANGE COLUMN `menu_id` `menu_id` INT(11) UNSIGNED");
            $table->foreign('menu_id')->references('menu_id')->on('menu');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu');
    }
}
