<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblNotification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification', function (Blueprint $table) {
            $table->increments('notification_id');
            $table->string('emai_to', 50);
            $table->string('email_from', 50);
            $table->string('email_subject', 100);
            $table->text('email_body');
            $table->dateTime('email_sent_date');
            $table->boolean('status');
            $table->boolean('is_active');
            $table->integer('update_by_user_id')->unsigned()->comment('FK:user>user_id')->nullable();
            $table->timestamp('updated_at');
            $table->integer('create_by_user_id')->unsigned()->comment('FK:user>user_id')->nullable();
            $table->timestamp('created_at')->useCurrent();
        });

        Schema::table('notification', function($table) {
            $table->foreign('create_by_user_id')->references('user_id')->on('user');
            $table->foreign('update_by_user_id')->references('user_id')->on('user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification');
    }
}
