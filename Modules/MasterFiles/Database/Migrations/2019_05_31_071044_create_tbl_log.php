<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log', function (Blueprint $table) {
            $table->increments('log_id');
            $table->integer('notification_id')->unsigned()->comment('FK:notification>notification_id')->nullable();
            $table->integer('reference_id')->comment('multi-purpose');
            $table->integer('menu_id')->unsigned()->comment('FK:menu>menu_id')->nullable();
            $table->integer('user_id')->unsigned()->comment('FK:user>user_id')->nullable();
            $table->string('contoller', 50);
            $table->string('action', 50);
            $table->timestamp('action_date');
            $table->boolean('is_active');
            $table->text('data');
            $table->integer('data_type');
        });

        Schema::table('log', function($table) {
            $table->foreign('notification_id')->references('notification_id')->on('notification');
            $table->foreign('menu_id')->references('menu_id')->on('menu');
            $table->foreign('user_id')->references('user_id')->on('user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log');
    }
}
