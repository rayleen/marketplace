<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblEmail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('email', function (Blueprint $table) {
            $table->string('email_username', 50);
            $table->string('email_password', 50);
            $table->string('smtp_host', 50);
            $table->boolean('is_active');
            $table->integer('smtp_port');
            
            $table->primary('email_username');
        });
;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email');
    }
}
