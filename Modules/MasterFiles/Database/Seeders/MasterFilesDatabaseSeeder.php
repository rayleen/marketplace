<?php

namespace Modules\MasterFiles\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MasterFilesDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $exist_country = DB::table('country')->exists();
        if(!$exist_country){
            $country_id = DB::table('country')->insertGetId([
                'name' => 'Malaysia',
                'code' => 'MY',
                'tel_country_code' => '60',
                'is_active' => 1
            ]);
        }

        $exist_state = DB::table('state')->exists();
        if(!$exist_state){
            $state_id = DB::table('state')->insert([
                'country_id' => $country_id,
                'name' => 'Penang',
                'is_active' => 1
            ]);
        }
        $exist_state = DB::table('postcode')->exists();
        if(!$exist_state){
            DB::table('postcode')->insert([
                'postcode' => '10250',
                'area_name' => 'Acardia Apartment',
                'state_id' => $state_id,
                'is_active' => 1
            ]);
        }

        $exist_mail = DB::table('email')->exists();
        if(!$exist_mail){
            DB::table('email')->insert([
                'email_username' => 'arms@arms.my`',
                'email_password' => 'paswrod123',
                'smtp_host' => 'smtp.host',
                'is_active' => 1,
                'smtp_port' => 25
            ]);
        }

        $menu = ['Dashboard', 'Products', 'Orders', 'Settings', 'Users', 'Reports', 'FAQ'];
        $submenus = ['Menu Theme', 'UI Theme',  'Branch', 'ARMS API', 'Lazada API'];

        for ($i = 0; $i < count($menu); $i++) {
            $exist_menu = DB::table('menu')->where('name', $menu[$i])->exists();
            if(!$exist_menu){
                DB::table('menu')->insertGetId([
                    'name' => $menu[$i],
                    'level' => 1,
                    'sequence' => ($i+1),
                    'is_active' => 1
                ]);
            }
        }  

    }
}
