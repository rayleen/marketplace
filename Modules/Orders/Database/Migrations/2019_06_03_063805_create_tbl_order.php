<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->increments('order_id');
            $table->integer('int_order_id')->unsigned();
            $table->integer('order_number');
            $table->timestampTz('int_created_at')->nullable();
            $table->timestampTz('int_updated_at')->nullable();
            $table->string('customer_first_name', 50);
            $table->string('customer_last_name', 50)->nullable();
            $table->string('status', 50);
            $table->double('price');
            $table->text('payment_method');
            $table->double('shipping_fee');
            $table->timestamp('promised_shipping_times')->nullable();
            $table->integer('items_count');
            $table->string('tax_code', 50);
            $table->double('voucher');
            $table->string('voucher_code', 50)->nullable();
            $table->string('gift_option', 10);
            $table->string('national_registration_number', 50)->nullable();
            $table->string('branch_number', 50)->nullable();
            $table->text('delivery_info')->nullable();
            $table->text('extra_attributes')->nullable();
            $table->string('billing_country', 50);
            $table->string('billing_address1', 50)->nullable();
            $table->string('billing_address2', 50)->nullable();
            $table->string('billing_address3', 50)->nullable();
            $table->string('billing_address4', 50)->nullable();
            $table->string('billing_address5', 50)->nullable();
            $table->string('billing_postcode', 10)->comment('FK:postcode>postcode')->nullable();
            $table->string('billing_city', 50)->nullable();
            $table->string('billing_phone', 50)->nullable();
            $table->string('billing_phone2', 50)->nullable();
            $table->string('billing_firstname', 50);
            $table->string('billing_lastname', 50);
            $table->string('billing_customer_email', 50);
            $table->string('shipping_country', 50);
            $table->string('shipping_address1', 50)->nullable();
            $table->string('shipping_address2', 50)->nullable();
            $table->string('shipping_address3', 50)->nullable();
            $table->string('shipping_address4', 50)->nullable();
            $table->string('shipping_address5', 50)->nullable();
            $table->string('shipping_postcode', 10)->comment('FK:postcode>postcode')->nullable();
            $table->string('shipping_city', 50);
            $table->string('shipping_phone', 50)->nullable();
            $table->string('shipping_phone2', 50)->nullable();
            $table->string('shipping_firstname', 50);
            $table->string('shipping_lastname', 50);
            $table->string('shipping_customer_email', 50);
            $table->string('do_no', 50);
            $table->dateTime('do_no_date')->nullable();
            $table->text('gift_message', 50)->nullable();
            $table->text('remarks', 50)->nullable();
            $table->boolean('is_active');
            $table->integer('update_by_user_id')->unsigned()->comment('FK:user>user_id')->nullable();
            $table->integer('create_by_user_id')->unsigned()->comment('FK:user>user_id')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('created_at')->useCurrent();

            $table->index('int_order_id');
        });

        Schema::table('order', function($table) {
            $table->foreign('billing_postcode')->references('postcode')->on('postcode');
            $table->foreign('shipping_postcode')->references('postcode')->on('postcode');
            $table->foreign('create_by_user_id')->references('user_id')->on('user');
            $table->foreign('update_by_user_id')->references('user_id')->on('user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
