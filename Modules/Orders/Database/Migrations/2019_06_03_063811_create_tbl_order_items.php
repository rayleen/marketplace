<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblOrderItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_item', function (Blueprint $table) {
            $table->increments('order_item_id'); 
            $table->integer('order_id')->unsigned()->comment('FK:order>order_id'); 
            $table->integer('int_order_id')->unsigned()->comment('FK:order>int_order_id'); 
            $table->integer('int_order_item_id')->unsigned();
            $table->double('paid_price');
            $table->text('product_main_image')->nullable();
            $table->double('tax_amount');
            $table->integer('voucher_platform');
            $table->text('reason')->nullable();
            $table->text('product_detail_url')->nullable();
            $table->timestamp('promised_shipping_time')->nullable();
            $table->integer('purchased_order_id')->nullable();
            $table->double('voucher_seller');
            $table->string('shipping_type', 50)->nullable();
            $table->timestampTz('int_created_at')->nullable();
            $table->string('voucher_code', 50)->nullable();
            $table->integer('package_id');
            $table->string('variation', 50);
            $table->double('wallet_credits');
            $table->timestampTz('int_updated_at')->nullable();
            $table->integer('purchased_order_number');
            $table->string('currency', 10);
            $table->string('shipping_provider_type', 50);
            $table->string('sku', 50)->comment('FK:product_sku>arms_marketplace_sku')->nullable();
            $table->string('invoice_number', 50);
            $table->string('cancel_return_initiator', 50)->nullable();
            $table->string('shop_sku', 50);
            $table->boolean('is_digital');
            $table->double('item_price');
            $table->double('shipping_service_cost');
            $table->string('tracking_code_pre', 50)->nullable();
            $table->string('tracking_code', 50)->nullable();
            $table->datetime('tracking_update_date');
            $table->boolean('tracking_update_status');
            $table->double('shipping_amount');
            $table->text('reason_detail')->nullable();
            $table->string('shop_id', 50)->nullable();
            $table->integer('return_status')->nullable();
            $table->string('name', 50)->nullable();
            $table->string('shipment_provider', 50)->nullable();
            $table->double('voucher_amount');
            $table->text('digital_delivery_info')->nullable();
            $table->text('extra_attributes')->nullable();
            $table->string('status', 50);
            $table->boolean('is_active');
            $table->integer('create_by_user_id')->unsigned()->comment('FK:user>user_id')->nullable();
            $table->integer('update_by_user_id')->unsigned()->comment('FK:user>user_id')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('created_at')->useCurrent();

            $table->index('sku');
            $table->index('int_order_id');
            $table->index('int_order_item_id');
        });

        Schema::table('order_item', function($table) {
            $table->foreign('order_id')->references('order_id')->on('order');
            $table->foreign('int_order_id')->references('int_order_id')->on('order');
            $table->foreign('sku')->references('arms_marketplace_sku')->on('product_sku');
            $table->foreign('create_by_user_id')->references('user_id')->on('user');
            $table->foreign('update_by_user_id')->references('user_id')->on('user');
        });
 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_item');
    }
}
