<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblTransactionDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_details', function (Blueprint $table) {
            $table->increments('transaction_details_id');
            $table->integer('int_order_id')->unsigned()->comment('FK:order_item>int_order_id'); 
            $table->integer('int_order_item_id')->unsigned()->comment('FK:order_item>int_order_item_id'); 
            $table->timestamp('transaction_date')->nullable();
            $table->double('amount');
            $table->boolean('paid_status');
            $table->string('shipping_provider', 50)->nullable();
            $table->boolean('WHT_included_in_amount');
            $table->string('lazada_sku', 50);
            $table->string('transaction_type', 50);
            $table->string('orderitem_status', 50);
            $table->string('reference', 50);
            $table->string('fee_name', 50);
            $table->string('shipping_speed', 50);
            $table->boolean('WHT_amount');
            $table->string('transaction_number');
            $table->string('seller_sku');
            $table->text('statement');
            $table->text('comment')->nullable();
            $table->text('details');
            $table->double('VAT_in_amount');
            $table->string('shipment_type');
            $table->boolean('is_active');
            $table->integer('create_by_user_id')->unsigned()->comment('FK:user>user_id')->nullable();
            $table->integer('update_by_user_id')->unsigned()->comment('FK:user>user_id')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('created_at')->useCurrent();

            $table->index('int_order_id');
            $table->index('int_order_item_id');
        });

        Schema::table('transaction_details', function($table) {
            $table->foreign('int_order_id')->references('int_order_id')->on('order_item');
            $table->foreign('int_order_item_id')->references('int_order_item_id')->on('order_item');
            $table->foreign('create_by_user_id')->references('user_id')->on('user');
            $table->foreign('update_by_user_id')->references('user_id')->on('user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_details');
    }
}
