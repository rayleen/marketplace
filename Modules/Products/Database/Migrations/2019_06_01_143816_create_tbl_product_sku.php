<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblProductSku extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_sku', function (Blueprint $table) {
            $table->increments('product_sku_id');
            $table->string('name', 50);
            $table->text('description');
            $table->integer('category_id')->unsigned()->comment('FK:category>category_id')->nullable();
            $table->integer('brand_id')->unsigned()->comment('FK:brand>brand_id')->nullable();
            $table->string('model', 50)->nullable();
            $table->string('color', 50)->nullable();
            $table->integer('size')->nullable();
            $table->double('price');
            $table->double('cost_price');
            $table->integer('quantity');
            $table->double('length')->nullable();
            $table->double('height')->nullable();
            $table->double('weight')->nullable();
            $table->double('width')->nullable();
            $table->integer('image_id')->unsigned()->comment('FK:image>image_id');
            $table->string('arms_marketplace_sku', 50);
            $table->string('arms_code', 50)->nullable();
            $table->string('sku_item_id', 50)->nullable();
            $table->string('mcode', 50)->nullable();
            $table->string('artno', 50)->nullable();
            $table->string('link_code', 50)->nullable();
            $table->boolean('promotion');
            $table->dateTime('promotion_date_from')->nullable();
            $table->dateTime('promotion_date_to')->nullable();
            $table->boolean('is_active');
            $table->integer('update_by_user_id')->unsigned()->comment('FK:user>user_id')->nullable();
            $table->integer('create_by_user_id')->unsigned()->comment('FK:user>user_id')->nullable();
            $table->timestamp('updated_at');
            $table->timestamp('created_at')->useCurrent();

            $table->index('arms_marketplace_sku');
        });


        Schema::table('product_sku', function($table) {
            $table->foreign('category_id')->references('category_id')->on('category');
            $table->foreign('brand_id')->references('brand_id')->on('brand');
            $table->foreign('image_id')->references('image_id')->on('image');
            $table->foreign('create_by_user_id')->references('user_id')->on('user');
            $table->foreign('update_by_user_id')->references('user_id')->on('user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_sku');
    }
}
