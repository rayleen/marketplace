<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblStockCountHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_count_histroy', function (Blueprint $table) {
            $table->increments('stock_count_history_id');
            $table->integer('product_sku_id');
            $table->integer('bf');
            $table->integer('qty');
            $table->text('remarks');
            $table->integer('cf');
            $table->timestamp('created_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_count_histroy');
    }
}
