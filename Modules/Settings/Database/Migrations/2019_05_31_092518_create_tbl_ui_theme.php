<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblUiTheme extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('ui_theme', function (Blueprint $table) {
            $table->increments('ui_theme_id');
            $table->string('theme_name', 50);
            $table->string('css_file_name', 50);
            $table->boolean('is_default');
            $table->boolean('is_active');
            $table->timestamp('updated_at');
            $table->timestamp('created_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ui_theme');
    }
}
