<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblBranch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch', function (Blueprint $table) {
            $table->increments('branch_id');
            $table->integer('company_id')->unsigned()->comment('FK:company>company_id')->nullable();
            $table->string('branch_name', 50);
            $table->boolean('intergrate_to_arms');
            $table->integer('ui_theme_id')->unsigned()->comment('FK:ui_theme>ui_theme_id')->nullable();
            $table->integer('menu_theme_id')->unsigned()->comment('FK:menu_theme>menu_theme_id')->nullable();
            $table->string('email', 50);
            $table->string('contact_name', 50);
            $table->string('contact_tel_no', 50);
            $table->string('address1', 50);
            $table->string('address2', 50)->nullable();
            $table->string('address3', 50)->nullable();
            $table->string('postcode', 10);
            $table->integer('state_id')->unsigned()->comment('FK:state>state_id')->nullable();
            $table->boolean('is_active');
            $table->integer('update_by_user_id')->unsigned()->comment('FK:user>user_id')->nullable();
            $table->integer('create_by_user_id')->unsigned()->comment('FK:user>user_id')->nullable();
            $table->timestamp('updated_at');
            $table->timestamp('created_at')->useCurrent();
        });

        Schema::table('branch', function($table) {
            $table->foreign('company_id')->references('company_id')->on('company');
            $table->foreign('postcode')->references('postcode')->on('postcode');
            $table->foreign('state_id')->references('state_id')->on('state');
            $table->foreign('create_by_user_id')->references('user_id')->on('user');
            $table->foreign('update_by_user_id')->references('user_id')->on('user');
        });

         Schema::table('user', function($table) {
            DB::statement("ALTER TABLE `user` CHANGE COLUMN `branch_id` `branch_id` INT(11) UNSIGNED");
            $table->foreign('branch_id')->references('branch_id')->on('branch');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch');
    }
}
