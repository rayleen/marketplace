<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlterTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('branch', function($table) {
            $table->dropForeign(['company_id']);
            $table->dropColumn(['company_id']);
            $table->renameColumn('branch_name', 'company_name');
            $table->string('branch_code', 50)->after('branch_id');
        });
        Schema::dropIfExists('company');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('');
    }
}
