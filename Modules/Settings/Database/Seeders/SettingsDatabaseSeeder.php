<?php

namespace Modules\Settings\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SettingsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $exist_branch = DB::table('branch')->exists();
        if(!$exist_branch){
            $branch_id = DB::table('branch')->insertGetId([
                'branch_code' => 'arms',
                'company_name' => 'ARMS',
                'intergrate_to_arms' => 0,
                'email' => 'armspenang@arms.my',
                'contact_name' => 'arms',
                'contact_tel_no' => 0,
                'postcode' => '10250',
                'address1' => 'address',
                'is_active' => 1
            ]);

            DB::table('user')->where('user_id', 1)->update([
                'branch_id' => $branch_id
            ]);

        }

        
        $theme_name = ['Grey', 'Orange'];
        $theme_file = ['grey-theme.css', 'orange-theme.css'];

       
        for ($i = 0; $i < count($theme_name); $i++) {
            $exist_theme = DB::table('ui_theme')->where('theme_name', $theme_name[$i])->exists();
            if(!$exist_theme){
                DB::table('ui_theme')->insertGetId([
                    'theme_name' => $theme_name[$i],
                    'css_file_name' => $theme_file[$i],
                    'is_default' => 0,
                    'is_active' => 1
                ]);

                DB::table('ui_theme')->where('ui_theme_id', 1)->update([
                    'is_default' => 1,
                ]);
            }
        }      
        
    }
}
