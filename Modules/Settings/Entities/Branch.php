<?php

namespace Modules\Settings\Entities;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $table = 'branch';
    protected $fillable = ['*'];

    public function company()
   	{
   		return $this->belongsTo('Modules\Settings\Entities\Company');
   	}

   
}
