<?php

namespace Modules\Settings\Entities;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'company';
    protected $fillable = ['*'];

   	public function branch()
   	{
   		return $this->hasMany('Modules\Settings\Entities\Branch');
   	}
}
