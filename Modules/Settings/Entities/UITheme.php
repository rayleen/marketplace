<?php

namespace Modules\Settings\Entities;

use Illuminate\Database\Eloquent\Model;

class UITheme extends Model
{
    protected $table = 'ui_theme';
    protected $fillable = ['*'];
    protected $primaryKey = 'ui_theme_id';

}
