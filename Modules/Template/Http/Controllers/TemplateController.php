<?php

namespace Modules\Template\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Settings\Entities\UITheme;

use Session;
use Redirect;
class TemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($ui_theme_id)
    {
        $olddefault = UITheme::where('is_default', 1)->first();
        $olddefault->is_default = 0;
        $olddefault->save();

        $default = UITheme::find($ui_theme_id);
        $css_name = $default->css_file_name;
        $default->is_default = 1;
        $default->save();

        Session::put('css_file_name', $css_name);

        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('template::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('template::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('template::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
    *Sample Layouts
    */
    public function layout_table()
    {
        return view('template::sample-layouts.table');
    }

    public function layout_form()
    {
        return view('template::sample-layouts.forms');
    }

    public function layout_modals()
    {
        return view('template::sample-layouts.modals');
    }
}
