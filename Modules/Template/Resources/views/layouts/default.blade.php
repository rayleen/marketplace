<!DOCTYPE html>
<html>
<head>
  <title>ARMS Marketplace</title>
   @include('template::layouts.includes.head')

   <!-- plugins css place after main css is loaded -->
    @yield('plugins-css')
    <link href="{{ asset('bootstrap') }}/dist/css/skins/main/{{ Session::get('css_file_name') }}" rel="stylesheet">
    <link href="{{ asset('bootstrap') }}/dist/css/skins/menu/normal.css" rel="stylesheet">
    

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
        <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
        <!--header start-->
        @include('template::layouts.includes.header')

        <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
        <!--sidebar start-->
         @include('template::layouts.navigation.left-sidebar-menu')
            @yield('sidebar-menu')
        
        <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
        <!--main content start-->
            @yield('content')

        <!-- **********************************************************************************************************************************************************
        MAIN FOOTER
        *********************************************************************************************************************************************************** -->
        <!--main footer start-->
        @include('template::layouts.includes.footer')

        <!-- **********************************************************************************************************************************************************
        MAIN CONTROLLER SIDEBAR
        *********************************************************************************************************************************************************** -->
        <!--main controler-sidebar start-->
        @include('template::layouts.includes.control-sidebar')

</div>
<!-- ./wrapper -->
 <!-- js placed at the end of the document so the pages load faster -->
        @include('template::layouts.includes.scripts') 

        <!-- plugins script place after maing js is loaded -->
        @yield('plugins-script')
</body>
</html>
