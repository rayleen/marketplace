<!DOCTYPE html>
<html>
<head>
  <title>ARMS Marketplace</title>
   @include('template::layouts.includes.head')

   <!-- plugins css place after main css is loaded -->
   @yield('plugins-css')
</head>
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">
        <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
        <!--header start-->
        @include('template::layouts.forgotpassword.includes.header')
        
        <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
        <!--main content start-->
            @yield('content')

        <!-- **********************************************************************************************************************************************************
        MAIN FOOTER
        *********************************************************************************************************************************************************** -->
        <!--main footer start-->
        @include('template::layouts.includes.footer')

        <!-- **********************************************************************************************************************************************************
        MAIN CONTROLLER SIDEBAR
        *********************************************************************************************************************************************************** -->
        <!--main controler-sidebar start-->
        @include('template::layouts.includes.control-sidebar')

</div>
<!-- ./wrapper -->
 <!-- js placed at the end of the document so the pages load faster -->
        @include('template::layouts.includes.scripts') 

        <!-- plugins script place after maing js is loaded -->
        @yield('plugins-script')
</body>
</html>
