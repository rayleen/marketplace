  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
   <!-- Bootstrap core CSS -->
  <link href="{{ asset('bootstrap') }}/login/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template -->
  <link href="{{ asset('bootstrap') }}/login/vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('bootstrap') }}/login/vendor/simple-line-icons/css/simple-line-icons.css">


  <!-- Custom styles for this template -->
  <link href="{{ asset('bootstrap') }}/login/css/new-age.min.css" rel="stylesheet">