@extends('template::layouts.default')

@section('plugins-css')
<link href="{{ asset('bootstrap') }}/dist/css/skins/main/orange-theme.css" rel="stylesheet">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet" href="{{ asset('bootstrap') }}/bower_components/bootstrap/dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="{{ asset('bootstrap') }}/bower_components/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="{{ asset('bootstrap') }}/bower_components/Ionicons/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('bootstrap') }}/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@stop

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
   <div class="container-fluid">
      @if($errors->any())
      <div class="alert alert-danger">
        <b>{{$errors->first()}}</h4>
      </div>
      @endif
    </div>
          <!-- /.container-fluid -->
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="box-header">
        <h3 class="box-title">Forms
          <small>sub-title</small>
        </h3>
        <!-- tools box -->
        <div class="pull-right box-tools">
          <button type="button" class="btn btn-block btn-primary">Action Button</button>
        </div>
        <!-- /. tools -->
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tabpane1" data-toggle="tab">Tabpane 1</a></li>
              <li><a href="#tabpane2" data-toggle="tab">Tabpane 2</a></li>
            </ul>
            <div class="tab-content">
	            <div class="tab-pane active" id="tabpane1">
	                <section id="new">
	                  <div class="box-body">
		                  	<form class="form-horizontal">
				              <div class="box-body">
				                  <div class="form-group row">
								    <label for="inputTextfield" class="col-sm-2 col-form-label">Textfield</label>
								    <div class="col-sm-4">
								      <input type="text" class="form-control" id="inputTextfield" placeholder="Textfield">
								    </div>

								    <label for="inputTextfield" class="col-sm-2 col-form-label">Textfield</label>
								    <div class="col-sm-4">
								      <input type="text" class="form-control" id="inputTextfield" placeholder="Textfield">
								    </div>
								  </div>
								  <div class="form-group row">
								    <label for="inputTextfield" class="col-sm-2 col-form-label">Textfield</label>
								    <div class="col-sm-4">
								      <input type="text" class="form-control" id="inputTextfield" placeholder="Textfield">
								    </div>
								    <label for="inputTextfield" class="col-sm-2 col-form-label">Textfield</label>
								    <div class="col-sm-4">
								      <input type="text" class="form-control" id="inputTextfield" placeholder="Textfield">
								    </div>
								  </div>
								  <div class="form-group row">
								    <label for="inputTextfield" class="col-sm-2 col-form-label">Textfield</label>
								    <div class="col-sm-10">
								      <input type="text" class="form-control" id="inputTextfield" placeholder="Textfield">
								    </div>
								  </div>
								  <!-- select -->
				                <div class="form-group row">
				                  <label for="inputTextfield" class="col-sm-2 col-form-label">Select</label>
				                  <div class="col-sm-10">
					                  <select class="form-control">
					                    <option>option 1</option>
					                    <option>option 2</option>
					                    <option>option 3</option>
					                    <option>option 4</option>
					                    <option>option 5</option>
					                  </select>
				              		</div>
				                </div>
				              </div>
				              <!-- /.box-body -->
				              <div class="box-footer">
				                <button type="submit" class="btn btn-warning">Action Button</button>
				                <button type="submit" class="btn btn-info pull-right">Action Button</button>
				              </div>
				              <!-- /.box-footer -->
	            		</form>
		               </div>
		            </section>  
		        </div>
              <div class="tab-pane" id="tabpane2">
                <div class="tab-pane" id="tabpane2">
	                <section id="new">
	                  <div class="box-body">
	                  	<div class="form-group row">
					    	<label for="inputTextfield" class="col-sm-2 col-form-label">Textfield</label>
						    <div class="col-sm-10">
						      <input type="text" class="form-control" id="inputTextfield" placeholder="Textfield">
						    </div>
					  	</div>
					  	<!-- Text Area -->
					  	<div class="form-group row">
		                  <label for="inputTextfield" class="col-sm-2 col-form-label">Textarea</label>
		                  	<div class="col-sm-10">
		                  		<textarea class="form-control" rows="3" placeholder="Enter ..."></textarea>
		              		</div>
                		</div>
		              </div>
		            </section>
		        </div>
              </div>
              <!-- /#ion-icons -->

            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

     <!-- Main content -->
    <section class="content">
    	<div class="row">
    		<div class="col-xs-12">
	    		<div class="box box-info">
	            
	            <div class="box-header with-border">
	              <h3 class="box-title">Horizontal Form</h3>
	            </div>
	            <!-- /.box-header -->
            		<!-- form start -->
            		<form class="form-horizontal">
			              <div class="box-body">
			                  <div class="form-group row">
							    <label for="inputTextfield" class="col-sm-2 col-form-label">Textfield</label>
							    <div class="col-sm-4">
							      <input type="text" class="form-control" id="inputTextfield" placeholder="Textfield">
							    </div>

							    <label for="inputTextfield" class="col-sm-2 col-form-label">Textfield</label>
							    <div class="col-sm-4">
							      <input type="text" class="form-control" id="inputTextfield" placeholder="Textfield">
							    </div>
							  </div>
							  <div class="form-group row">
							    <label for="inputTextfield" class="col-sm-2 col-form-label">Textfield</label>
							    <div class="col-sm-4">
							      <input type="text" class="form-control" id="inputTextfield" placeholder="Textfield">
							    </div>
							    <label for="inputTextfield" class="col-sm-2 col-form-label">Textfield</label>
							    <div class="col-sm-4">
							      <input type="text" class="form-control" id="inputTextfield" placeholder="Textfield">
							    </div>
							  </div>
							  <div class="form-group row">
							    <label for="inputTextfield" class="col-sm-2 col-form-label">Textfield</label>
							    <div class="col-sm-10">
							      <input type="text" class="form-control" id="inputTextfield" placeholder="Textfield">
							    </div>
							  </div>
							  <!-- select -->
			                <div class="form-group row">
			                  <label for="inputTextfield" class="col-sm-2 col-form-label">Select</label>
			                  <div class="col-sm-10">
				                  <select class="form-control">
				                    <option>option 1</option>
				                    <option>option 2</option>
				                    <option>option 3</option>
				                    <option>option 4</option>
				                    <option>option 5</option>
				                  </select>
			              		</div>
			                </div>
			              </div>
			              <!-- /.box-body -->
			              <div class="box-footer">
			                <button type="submit" class="btn btn-warning">Action Button</button>
			                <button type="submit" class="btn btn-info pull-right">Action Button</button>
			              </div>
			              <!-- /.box-footer -->
	            	</form>
	          </div>
			</div>
    	</div>
    </section>
  </div>
@stop

@section('plugins-script')
<script src="{{ asset('bootstrap') }}/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('bootstrap') }}/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="{{ asset('bootstrap') }}/bower_components/fastclick/lib/fastclick.js"></script>
<script>
  $(function () {
    $('#example1').dataTable()
  })
</script>
@stop