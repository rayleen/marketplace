@extends('template::layouts.default')

@section('plugins-css')
<link href="{{ asset('bootstrap') }}/dist/css/skins/main/orange-theme.css" rel="stylesheet">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet" href="{{ asset('bootstrap') }}/bower_components/bootstrap/dist/css/bootstrap.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="{{ asset('bootstrap') }}/bower_components/Ionicons/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('bootstrap') }}/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<!-- Drag and Drop Upload Image -->
<link rel="stylesheet" href="{{ asset('bootstrap') }}/plugins/upload-image/upload-image.css">

<!-- Do not copy this style, this is for removing of height only for this page -->
<style type="text/css">
	.content{
		min-height: 0;
	}
</style>
@stop

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
   <div class="container-fluid">
      @if($errors->any())
      <div class="alert alert-danger">
        <b>{{$errors->first()}}</h4>
      </div>
      @endif
    </div>
          <!-- /.container-fluid -->
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="box-header">
        <h3 class="box-title">Modals
          <small>sub-title</small>
        </h3>
      </div>
    </section>

     <!-- Main content -->
    <section class="content">
    	<div class="row">
        <div class="col-xs-12">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Modal Examples</h3>
            </div>
            <div class="box-body">
              <div class="col-md-2">
              <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-default">
                Launch Default Modal
              </button>
              </div>
              <div class="col-md-8">
              	<label>Modal Button:</label>
              	<textarea class="form-control" rows="3"><button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-default">Launch Default Modal</button></textarea>
              	<label>Modal Content:</label>
              	<textarea class="form-control" rows="22">
<div class="modal fade" id="modal-default">
	  <div class="modal-dialog">
		    <div class="modal-content">
			    <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title">Default Modal</h4>
			     </div>
				<div class="modal-body">
					<p>One fine body&hellip;</p>
				</div>
			    <div class="modal-footer">
			        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
			       	<button type="button" class="btn btn-primary">Save changes</button>
			    </div>
		    </div>
	    	<!-- /.modal-content -->
	  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
              	</textarea>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

	<!--Main Section -->
	<section class="content">
	<div class="row">
	<div class="col-xs-6">
        <div class="box box-default">
        <div class="box-header">
          <h3 class="box-title">Buttons for table column</h3>
        </div>
		<div class="box-body pad table-responsive">
			<!-- button type -->
          <table class="table table-bordered text-center">
          	<tr>
          		<th>Button Type</th>
          		<th>Code</th>
          	</tr>
            <tr>
              <td>
                <button type="button" class="btn btn-block btn-warning"><i class="fa fa-edit"></i> Edit</button>
              </td>
              <td>
              	<textarea class="form-control" rows="3"><button type="button" class="btn btn-block btn-warning"><i class="fa fa-edit"></i> Edit</button></textarea>
              </td>
            </tr>
            <tr>
            	<td>
            		<button type="button" class="btn btn-block btn-success"><i class="fa fa-plus"></i> Add</button>
            	</td>
              	<td>
              	<textarea class="form-control" rows="3" placeholder="Enter ..."><button type="button" class="btn btn-block btn-success"><i class="fa fa-plus"></i> Add</button></textarea>
              </td>
            </tr>
            <tr>
            	<td>
            		<button type="button" class="btn btn-block btn-info"><i class="fa fa-eye"></i> View</button>
            	</td>
              	<td>
              	<textarea class="form-control" rows="3" placeholder="Enter ..."><button type="button" class="btn btn-block btn-info"><i class="fa fa-eye"></i> View</button></textarea>
              </td>
            </tr>
            <tr>
            	<td>
            		<button type="button" class="btn btn-block btn-danger"><i class="fa fa-trash"></i> Delete</button>
            	</td>
             	<td>
              		<textarea class="form-control" rows="3" placeholder="Enter ..."><button type="button" class="btn btn-block btn-danger"><i class="fa fa-trash"></i> Delete</button></textarea>
              	</td>
            </tr>
          </table>
            <!-- a Tag buttons -->
           <table class="table table-bordered text-center">
          	<tr>
          		<th>a Tag<br>(<small>link pages</small>)</th>
          		<th>Code</th>
          	</tr>
            <tr>
              <td>
                <a class="btn btn-sm btn-warning"  href="#"  title="View"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a>
              </td>
              <td>
              	<textarea class="form-control" rows="3" placeholder="Enter ..."><a class="btn btn-sm btn-warning"  href="#"  title="View"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a></textarea>
              </td>
            </tr>
            <tr>
            	<td>
                	<a class="btn btn-sm btn-success"  href="#"  title="Add"><i class="fa fa-plus" aria-hidden="true"></i> Add</a>
              	</td>
              	<td>
              	<textarea class="form-control" rows="3" placeholder="Enter ..."><a class="btn btn-sm btn-success"  href="#"  title="Add"><i class="fa fa-plus" aria-hidden="true"></i> Add</a></textarea>
              </td>
            </tr>
            <tr>
            	<td>
                	<a class="btn btn-sm btn-info"  href="#"  title="View"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
              	</td>
              	<td>
              	<textarea class="form-control" rows="3" placeholder="Enter ..."><a class="btn btn-sm btn-info"  href="#"  title="View"><i class="fa fa-eye" aria-hidden="true"></i> View</a></textarea>
              </td>
            </tr>
            <tr>
            	<td>
                	<a class="btn btn-sm btn-danger"  href="#"  title="Delete"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
              	</td>
              	<td>
              	<textarea class="form-control" rows="3" placeholder="Enter ..."><a class="btn btn-sm btn-danger"  href="#"  title="Delete"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a></textarea>
              </td>
            </tr>
          </table>

        </div>
		</div>
	</div>

	<div class="col-xs-6">
        <div class="box box-default">
        <div class="box-header">
          <h3 class="box-title">Buttons for table column (Icons)</h3>
        </div>
		<div class="box-body pad table-responsive">
			<!-- button type -->
          <table class="table table-bordered text-center">
          	<tr>
          		<th>Button Type</th>
          		<th>Code</th>
          	</tr>
            <tr>
              <td>
              	<div class="col-md-4">
              		<button type="button" class="btn btn-block btn-warning"><i class="fa fa-edit"></i></button>
              	</div>
              </td>
              <td>
              	<textarea class="form-control" rows="3"><button type="button" class="btn btn-block btn-warning"><i class="fa fa-edit"></i></button></textarea>
              </td>
            </tr>
            <tr>
            	<td>
              	<div class="col-md-4">
              		<button type="button" class="btn btn-block btn-success"><i class="fa fa-plus"></i></button>
              	</div>
              	</td>
              	<td>
              	<textarea class="form-control" rows="3" placeholder="Enter ..."><button type="button" class="btn btn-block btn-success"><i class="fa fa-plus"></i></button></textarea>
              </td>
            </tr>
            <tr>
            	<td>
              	<div class="col-md-4">
              		<button type="button" class="btn btn-block btn-info"><i class="fa fa-eye"></i></button>
              	</div>
              	</td>
              	<td>
              	<textarea class="form-control" rows="3" placeholder="Enter ..."><button type="button" class="btn btn-block btn-info"><i class="fa fa-eye"></i></button></textarea>
              </td>
            </tr>
            <tr>
            	<td>
              	<div class="col-md-4">
              		<button type="button" class="btn btn-block btn-danger"><i class="fa fa-trash"></i></button>
              	</div>
             	</td>
             	<td>
              	<textarea class="form-control" rows="3" placeholder="Enter ..."><button type="button" class="btn btn-block btn-danger"><i class="fa fa-trash"></i></button></textarea>
              </td>
            </tr>
          </table>

          <!-- a Tag buttons -->
           <table class="table table-bordered text-center">
          	<tr>
          		<th>a Tag<br>(<small>link pages</small>)</th>
          		<th>Code</th>
          	</tr>
            <tr>
              <td>
                <a class="btn btn-sm btn-warning"  href="#"  title="View"><i class="fa fa-edit" aria-hidden="true"></i></a>
              </td>
              <td>
              	<textarea class="form-control" rows="3" placeholder="Enter ..."><a class="btn btn-sm btn-warning"  href="#"  title="View"><i class="fa fa-edit" aria-hidden="true"></i></a></textarea>
              </td>
            </tr>
            <tr>
              	<td>
                	<a class="btn btn-sm btn-success"  href="#"  title="Add"><i class="fa fa-plus" aria-hidden="true"></i></a>
              	</td>
              	<td>
              	<textarea class="form-control" rows="3" placeholder="Enter ..."><a class="btn btn-sm btn-success"  href="#"  title="Add"><i class="fa fa-plus" aria-hidden="true"></i></a></textarea>
              </td>
            </tr>
            <tr>
              	<td>
                	<a class="btn btn-sm btn-info"  href="#"  title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>
              	</td>
              	<td>
              	<textarea class="form-control" rows="3" placeholder="Enter ..."><a class="btn btn-sm btn-info"  href="#"  title="View"><i class="fa fa-eye" aria-hidden="true"></i></a></textarea>
              </td>
            </tr>
            <tr>
              	<td>
                	<a class="btn btn-sm btn-danger"  href="#"  title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
              	</td>
              	<td>
              	<textarea class="form-control" rows="3" placeholder="Enter ..."><a class="btn btn-sm btn-danger"  href="#"  title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a></textarea>
              </td>
            </tr>
          </table>
        </div>
		</div>
	</div>
	</div>
	</section>

	<!--Main Section -->
	<section class="content">
	<div class="row">
	<div class="col-xs-12">
        <div class="box box-default">
        <div class="box-header">
          <h3 class="box-title">Form Buttons</h3>
        </div>
		<div class="box-body pad table-responsive">
          <table class="table table-bordered text-center">
          	<tr>
          		<th></th>
          		<th>Code</th>
          	</tr>
            <tr>
              <td>
                <button type="button" class="btn btn-block btn-primary"> Save</button>
              </td>
              <td>
              	<textarea class="form-control" rows="3"><button type="button" class="btn btn-block btn-primary"> Save</button></textarea>
              </td>
            </tr>
            <tr>
              <td>
                <button type="button" class="btn btn-block btn-warning"> Cancel</button>
              </td>
              <td>
              	<textarea class="form-control" rows="3"><button type="button" class="btn btn-block btn-warning"> Cancel</button></textarea>
              </td>
          	</tr>
          	<tr>
              <td>
                <button type="button" class="btn btn-block btn-success"> Search</button>
              </td>
              <td>
              	<textarea class="form-control" rows="3"><button type="button" class="btn btn-block btn-success"> Search</button></textarea>
              </td>
            </tr>
          </table>
        </div>
		</div>
	</div>
	</div>
	</section>

    <!--Main Section -->
	<section class="content">
    	<div class="row">
    	<div class="col-xs-12">
	        <div class="box box-default">
	        <div class="box-header">
              <h3 class="box-title">Button Size</h3>
            </div>
			<div class="box-body pad table-responsive">
              <p>Various types of buttons. Using the base class <code>.btn</code></p>
              <table class="table table-bordered text-center">
                <tr>
                  <th>Normal</th>
                  <th>Large <code>.btn-lg</code></th>
                  <th>Small <code>.btn-sm</code></th>
                  <th>X-Small <code>.btn-xs</code></th>
                  <th>Flat <code>.btn-flat</code></th>
                  <th>Disabled <code>.disabled</code></th>
                </tr>
                <tr>
                  <td>
                    <button type="button" class="btn btn-block btn-default">Default</button>
                  </td>
                  <td>
                    <button type="button" class="btn btn-block btn-default btn-lg">Default</button>
                  </td>
                  <td>
                    <button type="button" class="btn btn-block btn-default btn-sm">Default</button>
                  </td>
                  <td>
                    <button type="button" class="btn btn-block btn-default btn-xs">Default</button>
                  </td>
                  <td>
                    <button type="button" class="btn btn-block btn-default btn-flat">Default</button>
                  </td>
                  <td>
                    <button type="button" class="btn btn-block btn-default disabled">Default</button>
                  </td>
                </tr>
                <tr>
                  <td>
                    <button type="button" class="btn btn-block btn-primary">Primary</button>
                  </td>
                  <td>
                    <button type="button" class="btn btn-block btn-primary btn-lg">Primary</button>
                  </td>
                  <td>
                    <button type="button" class="btn btn-block btn-primary btn-sm">Primary</button>
                  </td>
                  <td>
                    <button type="button" class="btn btn-block btn-primary btn-xs">Primary</button>
                  </td>
                  <td>
                    <button type="button" class="btn btn-block btn-primary btn-flat">Primary</button>
                  </td>
                  <td>
                    <button type="button" class="btn btn-block btn-primary disabled">Primary</button>
                  </td>
                </tr>
                <tr>
                  <td>
                    <button type="button" class="btn btn-block btn-success">Success</button>
                  </td>
                  <td>
                    <button type="button" class="btn btn-block btn-success btn-lg">Success</button>
                  </td>
                  <td>
                    <button type="button" class="btn btn-block btn-success btn-sm">Success</button>
                  </td>
                  <td>
                    <button type="button" class="btn btn-block btn-success btn-xs">Success</button>
                  </td>
                  <td>
                    <button type="button" class="btn btn-block btn-success btn-flat">Success</button>
                  </td>
                  <td>
                    <button type="button" class="btn btn-block btn-success disabled">Success</button>
                  </td>
                </tr>
                <tr>
                  <td>
                    <button type="button" class="btn btn-block btn-info">Info</button>
                  </td>
                  <td>
                    <button type="button" class="btn btn-block btn-info btn-lg">Info</button>
                  </td>
                  <td>
                    <button type="button" class="btn btn-block btn-info btn-sm">Info</button>
                  </td>
                  <td>
                    <button type="button" class="btn btn-block btn-info btn-xs">Info</button>
                  </td>
                  <td>
                    <button type="button" class="btn btn-block btn-info btn-flat">Info</button>
                  </td>
                  <td>
                    <button type="button" class="btn btn-block btn-info disabled">Info</button>
                  </td>
                </tr>
                <tr>
                  <td>
                    <button type="button" class="btn btn-block btn-danger">Danger</button>
                  </td>
                  <td>
                    <button type="button" class="btn btn-block btn-danger btn-lg">Danger</button>
                  </td>
                  <td>
                    <button type="button" class="btn btn-block btn-danger btn-sm">Danger</button>
                  </td>
                  <td>
                    <button type="button" class="btn btn-block btn-danger btn-xs">Danger</button>
                  </td>
                  <td>
                    <button type="button" class="btn btn-block btn-danger btn-flat">Danger</button>
                  </td>
                  <td>
                    <button type="button" class="btn btn-block btn-danger disabled">Danger</button>
                  </td>
                </tr>
                <tr>
                  <td>
                    <button type="button" class="btn btn-block btn-warning">Warning</button>
                  </td>
                  <td>
                    <button type="button" class="btn btn-block btn-warning btn-lg">Warning</button>
                  </td>
                  <td>
                    <button type="button" class="btn btn-block btn-warning btn-sm">Warning</button>
                  </td>
                  <td>
                    <button type="button" class="btn btn-block btn-warning btn-xs">Warning</button>
                  </td>
                  <td>
                    <button type="button" class="btn btn-block btn-warning btn-flat">Warning</button>
                  </td>
                  <td>
                    <button type="button" class="btn btn-block btn-warning disabled">Warning</button>
                  </td>
                </tr>
                <tr>
                	<td>
                		<textarea class="form-control" rows="2"><button type="button" class="btn btn-block btn-primary ">Primary</button></textarea>
                	</td>
                	<td>
                		<textarea class="form-control" rows="2">btn-lg</textarea>
                	</td>
                	<td>
                		<textarea class="form-control" rows="2">btn-sm</textarea>
                	</td>
                	<td>
                		<textarea class="form-control" rows="2">btn-xs</textarea>
                	</td>
                	<td>
                		<textarea class="form-control" rows="2">btn-flat</textarea>
                	</td>
                	<td>
                		<textarea class="form-control" rows="2">disabled</textarea>
                	</td>
                </tr>
              </table>
            </div>
			</div>
		</div>
		</div>
		</section>

		 <!--Main Section -->
		<section class="content">
	    	<div class="row">
		        <div class="col-xs-6">
		        	 <div class="box box-default">
				        <div class="box-header">
			             <!--  <i class="fa fa-edit"></i> -->
			              <h3 class="box-title">General Elements</h3>
			            </div>
			            <div class="box-body">
			              <form role="form">
			                <!-- text input -->
			                <div class="form-group">
			                  <label>Text</label>
			                  <input type="text" class="form-control" placeholder="Enter ...">
			                  <br>
			                  <label>Code</label>
			                  <textarea class="form-control" rows="2"><input type="text" class="form-control" placeholder="Enter ..."></textarea>
			                </div>
			                <hr>
			                <div class="form-group">
			                  <label>Text Disabled</label>
			                  <input type="text" class="form-control" placeholder="Enter ..." disabled>
			                  <br>
			                  <label>Code</label>
			                  <textarea class="form-control" rows="2"><input type="text" class="form-control" placeholder="Enter ..." disabled></textarea>
			                </div>
			                <hr>
			                <!-- textarea -->
			                <div class="form-group">
			                  <label>Textarea</label>
			                  <textarea class="form-control" rows="3" placeholder="Enter ..."></textarea>
			                  <br>
			                  <label>Code</label>
			                  <textarea class="form-control" rows="2"><textarea class="form-control" rows="3" placeholder="Enter ..."></textarea></textarea>
			                </div>
			                <hr>
			                <div class="form-group">
			                  <label>Textarea Disabled</label>
			                  <textarea class="form-control" rows="3" placeholder="Enter ..." disabled></textarea>
			                  <br>
			                  <label>Code</label>
			                  <textarea class="form-control" rows="2"><textarea class="form-control" rows="3" placeholder="Enter ..." disabled></textarea></textarea>
			                </div>
			                <hr>
			                <!-- checkbox -->
			                <div class="box-header">
				              <h3 class="box-title">Checkboxes</h3>
				            </div>
			                <label>Default</label>

			                <div class="form-group">
			                  <div class="checkbox">
			                    <label>
			                      <input type="checkbox">
			                      Checkbox 1
			                    </label>
			                  </div>

			                  <div class="checkbox">
			                    <label>
			                      <input type="checkbox">
			                      Checkbox 2
			                    </label>
			                  </div>

			                  <div class="checkbox">
			                    <label>
			                      <input type="checkbox" disabled>
			                      Checkbox disabled
			                    </label>
			                  </div>
			                </div>
			                <label>Code</label>
			                  <textarea class="form-control" rows="9">
<div class="form-group">
  <div class="checkbox">
    <label>
      <input type="checkbox">
      Checkbox 1
    </label>
  </div>

  <div class="checkbox">
    <label>
      <input type="checkbox">
      Checkbox 2
    </label>
  </div>

  <div class="checkbox">
    <label>
      <input type="checkbox" disabled>
      Checkbox disabled
    </label>
  </div>
</div>
			                  </textarea>

			                  <hr>
			                <!-- checkbox inline-->
			                <label>Inline</label>
			                <div class="form-group">
			                <div class="checkbox-inline">
							  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
							  <label class="form-check-label" for="inlineCheckbox1">1</label>
							</div>
							<div class="checkbox-inline">
							  <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
							  <label class="form-check-label" for="inlineCheckbox2">2</label>
							</div>
							<div class="checkbox-inline">
							  <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3" disabled>
							  <label class="form-check-label" for="inlineCheckbox3">3 (disabled)</label>
							</div>
							</div>
 							<label>Code</label>
			                <textarea class="form-control" rows="9">
<div class="form-group">
<div class="checkbox-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
  <label class="form-check-label" for="inlineCheckbox1">1</label>
</div>
<div class="checkbox-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
  <label class="form-check-label" for="inlineCheckbox2">2</label>
</div>
<div class="checkbox-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3" disabled>
  <label class="form-check-label" for="inlineCheckbox3">3 (disabled)</label>
</div>
</div>
			               	</textarea>
							<hr>
			                <!-- radio buttons -->
			                <div class="box-header">
				              <h3 class="box-title">Radio Buttons</h3>
				            </div>
				            <label>Default</label>
			                <div class="form-group">
			                  <div class="radio">
			                    <label>
			                      <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
			                      Option one is this and that&mdash;be sure to include why it's great
			                    </label>
			                  </div>
			                  <div class="radio">
			                    <label>
			                      <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
			                      Option two can be something else and selecting it will deselect option one
			                    </label>
			                  </div>
			                  <div class="radio">
			                    <label>
			                      <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3" disabled>
			                      Option three is disabled
			                    </label>
			                  </div>
			                </div>
							<label>Code</label>
			                <textarea class="form-control" rows="9">
<div class="form-group">
  <div class="radio">
    <label>
      <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
      Option one is this and that&mdash;be sure to include why it's great
    </label>
  </div>
  <div class="radio">
    <label>
      <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
      Option two can be something else and selecting it will deselect option one
    </label>
  </div>
  <div class="radio">
    <label>
      <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3" disabled>
      Option three is disabled
    </label>
  </div>
</div>	
			                </textarea>

			                <hr>
			                <label>Inline</label>
			                <div class="form-group">
			                  <div class="form-check checkbox-inline">
								  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
								  <label class="form-check-label" for="inlineRadio1">1</label>
								</div>
								<div class="form-check checkbox-inline">
								  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
								  <label class="form-check-label" for="inlineRadio2">2</label>
								</div>
								<div class="form-check checkbox-inline">
								  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3" disabled>
								  <label class="form-check-label" for="inlineRadio3">3 (disabled)</label>
								</div>
			                </div>
							<label>Code</label>
			                <textarea class="form-control" rows="9">
<div class="form-group">
  <div class="form-check checkbox-inline">
	  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
	  <label class="form-check-label" for="inlineRadio1">1</label>
	</div>
	<div class="form-check checkbox-inline">
	  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
	  <label class="form-check-label" for="inlineRadio2">2</label>
	</div>
	<div class="form-check checkbox-inline">
	  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3" disabled>
	  <label class="form-check-label" for="inlineRadio3">3 (disabled)</label>
	</div>
</div>
			                </textarea>
			                <!-- select -->
			                <div class="form-group">
			                  <label>Select</label>
			                  <select class="form-control">
			                    <option>option 1</option>
			                    <option>option 2</option>
			                    <option>option 3</option>
			                    <option>option 4</option>
			                    <option>option 5</option>
			                  </select>
			                </div>
			                <label>Code</label>
			                <textarea class="form-control" rows="9">
<div class="form-group">
  <label>Select</label>
  <select class="form-control">
    <option>option 1</option>
    <option>option 2</option>
    <option>option 3</option>
    <option>option 4</option>
    <option>option 5</option>
  </select>
</div>
			                </textarea>
			              </form>
			            </div>
			            <!-- /.box-body -->
			         </div>
		        </div>

		        <!-- DRAG AND DROP UPLOAD IMAGE -->
		         <div class="col-xs-6">
		        	 <div class="box box-default">
				        <div class="box-header">
			             <!--  <i class="fa fa-edit"></i> -->
			              <h3 class="box-title">Upload Image</h3>
			            </div>
			            <div class="box-body">
				          <!-- Standar Form -->
						   	<div class="col-md-12">
						      <form method="post" action="#" id="#">
					              <div class="form-group files color">
					               <input type="file" id="exampleInputFile">
					              </div>
					          </form>
				            </div>
				         <label>Code</label>
			             <textarea class="form-control" rows="9">
<div class="box box-default">
    <div class="box-header">
     <!--  <i class="fa fa-edit"></i> -->
      <h3 class="box-title">Upload Image</h3>
    </div>
    <div class="box-body">
      <!-- Standar Form -->
	   	<div class="col-md-12">
	      <form method="post" action="#" id="#">
              <div class="form-group files color">
               <input type="file" id="exampleInputFile">
              </div>
          </form>
        </div>
	</div>
</div>
			             </textarea>
			        	</div>
			    	</div>
		    	</div>
		</section>
  </div>



  <div class="modal fade" id="modal-default">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Default Modal</h4>
	      </div>
	      <div class="modal-body">
	        <p>One fine body&hellip;</p>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-primary">Save changes</button>
	      </div>
	    </div>
	    <!-- /.modal-content -->
	  </div>
	  <!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->



@stop

@section('plugins-script')
<script src="{{ asset('bootstrap') }}/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('bootstrap') }}/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="{{ asset('bootstrap') }}/bower_components/fastclick/lib/fastclick.js"></script>
<!-- Drag and Drop Upload Image -->
<script src="{{ asset('bootstrap') }}/plugins/upload-image/upload-image.js"></script>
<script>
  $(function () {
    $('#example1').dataTable()
  })
</script>
@stop