<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('template')->group(function() {

    Route::get('/', 'TemplateController@index');
    Route::get('/table', 'TemplateController@layout_table')->name('layout.table');
    Route::get('/forms', 'TemplateController@layout_form')->name('layout.form');
    Route::get('/modals', 'TemplateController@layout_modals')->name('layout.modals');
    Route::get('/change_skin/{id}', 'TemplateController@index')->name('change_skin');
});
