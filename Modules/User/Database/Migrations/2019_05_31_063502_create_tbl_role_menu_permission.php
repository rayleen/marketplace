<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblRoleMenuPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_menu_permission', function (Blueprint $table) {
            $table->increments('role_menu_permision_id');
            $table->tinyInteger('permission_id')->comment('FK:permission>permission_id');
            $table->integer('role_menu_id')->unsigned()->comment('FK:role_menu>role_menu_id');
            $table->boolean('is_active');
            $table->integer('update_by_user_id')->unsigned()->comment('FK:user>user_id')->nullable();
            $table->timestamp('updated_at');
            $table->integer('create_by_user_id')->unsigned()->comment('FK:user>user_id')->nullable();
            $table->timestamp('created_at')->useCurrent();
        });

        Schema::table('role_menu_permission', function($table) {
            $table->foreign('role_menu_id')->references('role_menu_id')->on('role_menu');
            $table->foreign('create_by_user_id')->references('user_id')->on('user');
            $table->foreign('update_by_user_id')->references('user_id')->on('user');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_menu_permission');
    }
}
