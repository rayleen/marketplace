<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTblUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tables = ['notification',
                    'order',
                    'order_item',
                    'permission',
                    'postcode',
                    'product_sku',
                    'role',
                    'role_menu',
                    'role_menu_permission',
                    'state',
                    'transaction_details',
                    'user',
                    'arms_api',
                    'branch',
                    'brand',
                    'category',
                    'country',
                    'image',
                    'marketplace_api',
                    'menu'];
                    
        foreach ($tables as $t) {
            Schema::table($t, function($table) {
                $table->renameColumn('update_by_user_id', 'updated_by_user_id');
                $table->renameColumn('create_by_user_id', 'created_by_user_id');
            });
        }
      


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
