<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $exist_role = DB::table('role')->exists();
        if(!$exist_role){
            DB::table('role')->insert([
                'name' => 'Superadmin',
                'is_active' => 1
            ]);
        }
        $exist_user = DB::table('user')->exists();
        if(!$exist_user){
            DB::table('user')->insert([
                'login_name' => 'admin',
                'password' => bcrypt('admin'),
                'first_name' => 'admin',
                'last_name' => 'admin',
                'email' => 'admin@gmail.com',
                'is_active' => 1,
                'role_id' => 1,
                'create_by_user_id' => null,
                'update_by_user_id' => null
            ]);
        }

    }
}
