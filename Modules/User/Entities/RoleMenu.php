<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;

class RoleMenu extends Model
{
    protected $table = 'role_menu';
    protected $primaryKey = 'role_id';

    protected $fillable = ['*'];

}
