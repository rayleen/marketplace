<?php

namespace Modules\User\Entities;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
	protected $table = 'user';
    protected $primaryKey = 'user_id';

    protected $fillable = ['*'];

    protected $hidden = ['password'];

    public function branch()
   	{
   		return $this->hasOne('Modules\Settings\Entities\Branch', 'branch_id');
   	}

}
