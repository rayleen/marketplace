<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Mail;
use Redirect;
use Hash;
use Config;
use DB;
use Validator;
use Auth;


use Modules\User\Entities\User;

class ForgotPasswordController extends Controller
{
    public function index()
    {
        return view('user::reset.index');
    }

    public function sendmail(Request $request)
    {
        /**
         * Check if email table is not empty
         * Email configurations in Login/Providers/MailServiceProvide
         */
        $mail = DB::table('email')->where('is_active', 1)->first();
        if ($mail) 
        {     
            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
            ]);

            if($validator->fails()){
                $validator->getMessageBag()->add('validation', 'Invalid email address.');
                return redirect('/')->withErrors($validator);
            }else{
                $email = $request->email;
                $user = User::where('email', '=', $email)->first();
                if($user){
                    $newpassword = str_random(8);
                    $mailsent = Mail::send('user::reset.email', ['user' => $user, 'newpassword' => $newpassword], function ($m) use ($user, $email) {
                        $m->replyTo('armsnoreply@gmail.com', 'ARMS Marketplace');
                        $m->from('armsnoreply@gmail.com', 'ARMS Marketplace Password Reset');
                        $m->to($email, $user['login_name'])->subject('ARMS Marketplace Password Reset');
                    });    

                    if (Mail::failures()) {
                        return Redirect::to('login')->withErrors('Email not sent! Please try again.');
                    }

                    $upUser = User::find($user->user_id);
                    $upUser->password = Hash::make($newpassword);
                    $upUser->save();
                    return Redirect::to('login')->with('message','Email successfully sent! Please check your email.');

                }else{
                    return Redirect::to('forgot_password')->withErrors('Email not found. Please make sure that you entered your correct email address.');
                }
            }
        }else{
            return Redirect::to('forgot_password')->withErrors('Please check email configuration first.');
        }
    }

    public function changepassword(){
        $user_id = encrypt(Auth::user()->user_id);
        return view('user::reset.changepassword', ['user_id' => $user_id]);
    }

    public function reset_password(Request $request){
        $user_id = decrypt($request->user_id); 
        $oldpassword = $request->oldpassword;

        $password = $request->newpassword;
        $user = User::find($user_id);
        $user_oldpassword = $user->password;

        $validator = Validator::make($request->all(), [
            'oldpassword' => 'required',
            'newpassword' => 'required|min:8|regex:/^[a-zA-Z0-9-_ ]*$/',
            'confirmpassword' => 'same:newpassword',
        ]);

        if($validator->fails()){
            return Redirect::back()->withErrors($validator->messages());
        }else{
            if(!Hash::check($oldpassword, $user_oldpassword)){            
                return Redirect::back()->withErrors('The specified password does not match your old password. Please try again.');
            }else{
                $user->password = Hash::make($password);
                $user->save();
                return Redirect::back()->with('message','Password changed successfully.');
            }
        }

       
    }
}
