<?php

namespace Modules\User\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

use DB;
use Config;
use Redirect;
class MailServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        if(Schema::hasTable('email')){
            $mail = DB::table('email')->where('is_active', 1)->first();
            if ($mail) 
            {        
                $config = array(
                    'driver'     => 'smtp',
                    'host'       => $mail->smtp_host,
                    'port'       => $mail->smtp_port,
                    'from'       => array('address' => 'armsnoreply@gmail.com', 'name' => 'ARMS'),
                    'encryption' => 'tls',
                    'username'   => $mail->email_username,
                    'password'   => $mail->email_password,
                    'sendmail'   => '/usr/sbin/sendmail -bs',
                    'pretend'    => false,
                );
                Config::set('mail', $config);
            }
        }    
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
