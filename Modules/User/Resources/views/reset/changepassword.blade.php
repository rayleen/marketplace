@extends('template::layouts.default')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
   <div class="container-fluid">
      @if($errors->any())
      <div class="alert alert-danger">
        <b>{{$errors->first()}}</h4>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      @endif

      @if(session()->has('message'))
          <div class="alert alert-success">
              {{ session()->get('message') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
          </div>
      @endif
    </div>
          <!-- /.container-fluid -->
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Change Password</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form method="POST" action="{{route('reset_password')}}">
          <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
          <div class="box-body">
            <div class="error-messages">

            </div>
              
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Old Password</label>
              <div class="col-sm-10">
                <input type="password" id="inputEmail" name="oldpassword" class="form-control" required="required" autofocus="autofocus">
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">New Password</label>
              <div class="col-sm-10">
                <input type="password" id="newPassword" name="newpassword" class="form-control" required="required" autofocus="autofocus">
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Confirm New Password</label>
              <div class="col-sm-10">
                 <input type="password" id="confirmnewPassword" name="confirmpassword" class="form-control" required="required" autofocus="autofocus" >
                  <input name="user_id" type="hidden" value="{{$user_id}}">
              </div>
            </div>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <button type="button" class="btn btn-default">Cancel</button>
            <button type="submit" name="submit" class="btn btn-info pull-right" id="btnsubmit" >Submit</button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>

    </section
    <!-- /.content -->
  </div>
@include('template::layouts.includes.scripts')
@include('user::reset.js.scripts')

@stop
