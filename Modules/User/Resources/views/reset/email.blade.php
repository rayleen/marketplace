<body>
	<div>
		Dear Valued Customer,
		<br>
		<br>
		Thank you for contacting ARMS. <br>
		This is an automated message that is sent to you after you have selected the 'Forgot Password' option.

		<p>Your User ID is <b style="font-size: 14px;">{{$user->login_name}}</b></p>
		<p>Your new password is <b style="font-size: 14px;">{{$newpassword}}</b></p>

		<p>Please carry out the following steps:</p>
		1) Go to ARMS Marketplace Login page<br>
		https://www/arms_marketplace/login<br>

		2) Enter your User ID and the given password<br>

		3) Once you have successfully logged in, please change to a new password immediately. (Password can only contain letters, numbers and underscores with minimum password length of 8 characters and must be a combination of letters and numeric).<br><br>

		As your password is highly confidential, please ensure you safeguard it and do not reveal to anyone.<br>

		Thank you.<br><br>

		<p>Sincerely,<br>
		ARMS Marketplace </p>
	</div>
     
</body>