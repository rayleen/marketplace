@extends('template::layouts.forgotpassword.reset-pass-mainlayout')

@section('plugins-css')
<link href="{{ asset('bootstrap') }}/dist/css/skins/main/orange-theme.css" rel="stylesheet">
@stop

@section('content')
   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <div class="container">
       <div class="container-fluid">
          @if($errors->any())
          <div class="alert alert-danger">
            <b>{{$errors->first()}}</h4>
          </div>
          @endif
        </div>
              <!-- /.container-fluid -->
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <h1>
          Reset Password
          <!-- <small>Example 2.0</small> -->
        </h1>
      </section>

        <!-- Main content -->
        <section class="content">
          <div class="card-body">
<!--                   <form method="POST" action="{{route('sendmail')}}">
                      <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                      <div class="form-group">
                        <div class="form-label-group">
                          <input type="text" id="inputEmail" name="email" class="form-control" required="required" autofocus="autofocus">
                          <label for="inputEmail">Email address</label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Submit</button>
                  </form> -->

                  <div class="login-box">
                      <div class="login-box-body">
                        <div class="register-logo">
                          <b>Trouble Logging In?</b>
                        </div>
                        <p class="login-box-msg">Enter your email and we'll send you a link to get back into your account.</p>

                        <form method="POST" action="{{route('sendmail')}}">
                          <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                          <div class="form-group has-feedback">
                            <input type="text" id="inputEmail"  name="email" class="form-control" required="required" autofocus="autofocus" placeholder="Email address">
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                          </div>
                           <button type="submit" class="btn btn-primary btn-block">Submit</button>
                        </form>
                      </div>
                  </div>
                  <!-- /.login-box-body -->
              </div>
        </section>
        <!-- /.content -->
      </div>
      <!-- /.container -->
      </div>
@stop
