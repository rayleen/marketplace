<script type="text/javascript">

$('#newPassword, #confirmnewPassword').on('keyup', function(event){
	var val = $(this).val();
	var error = '';
	if(val.length < 8){
		error += '<p>Password should be at least 8 characters.</p>';
	}else{
		error += '';
	}

	if(val.match(/[0-9]+/)){
		error += '';
	}else{
		error += '<p>Password should contains at least 1 numeric character.</p>';
	}

	if(val.match(/[a-z]+/) || val.match(/[A-Z]+/)) {
		error += '';
	}else{
		error += '<p>Password should contains at least 1 alpha character.</p>';
	}

	if(/^[a-zA-Z0-9-_ ]*$/.test(val) == false) {
		error += '<p>Only underscores (_) are allowed special characters.</p>';
	}else{
		error += '';
	}

	var pw = $('#newPassword').val();
	var cp = $('#confirmnewPassword').val();

	if(cp.length > 0){
		if(pw != cp){
			error += '<p>Password mismatch.</p>';
		}else{
			error += '';
		}
	}

	$('.error-messages').html(error);
	if(error == ''){
		$('#btnsubmit').removeAttr('disabled');
		$('.error-messages').removeClass('alert alert-danger');
	}else{
		$('#btnsubmit').attr('disabled', true);
		$('.error-messages').addClass('alert alert-danger');
	}


});

// $('#confirmnewPassword').on('keyup', function(){
// 	var val = $(this).val();
// 	var pw = $('#newPassword').val();
// 	var error = '';
// 	if(val != pw){
// 		error += '<p>Password mismatch.</p>';
// 	}else{
// 		error += '';
// 	}

// 	$('.error-messages').html(error);
// 	if(error == ''){
// 		$('#btnsubmit').removeAttr('disabled');
// 		$('.error-messages').removeClass('alert alert-danger');
// 	}else{
// 		$('#btnsubmit').attr('disabled', true);
// 		$('.error-messages').addClass('alert alert-danger');
// 	}
// });



</script>