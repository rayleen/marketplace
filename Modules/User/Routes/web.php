<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'web', 'namespace' => '\Modules\User\Http\Controllers', 'prefix' => 'user'], function()
{
	Route::get('/forgot_password', 'ForgotPasswordController@index')->name('forgot_password');
	Route::post('/sendmail', 'ForgotPasswordController@sendmail')->name('sendmail');
	Route::get('change_password', 'ForgotPasswordController@changepassword')->name('changepassword');
	Route::post('reset_password', 'ForgotPasswordController@reset_password')->name('reset_password');


});


