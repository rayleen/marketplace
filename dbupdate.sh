php artisan module:migrate User;
php artisan module:migrate Products;

php artisan module:seed User;
php artisan module:seed MasterFiles;
php artisan module:seed Settings;
php artisan module:seed API;
